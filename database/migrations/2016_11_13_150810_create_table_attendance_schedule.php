<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttendanceSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('attendance_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendar_id');
            $table->string('name');
            $table->time('start_time');
            $table->time('end_time');
            $table->boolean('schedule_attendance');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('attendance_schedule');
    }
}
