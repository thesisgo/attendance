<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClassAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('class_attendance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('attendance_date');
            $table->time('attendance_time');
            $table->integer('status');
            $table->string('remarks');
            $table->integer('calendar_id');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('class_attendance');
    }
}
